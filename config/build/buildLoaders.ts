import type webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { type BuildOptions } from './types/types';

export function buildLoaders(options: BuildOptions): webpack.RuleSetRule[] {
    const isDev = options.mode === 'development';

    const typeScriptLoader = {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
    };

    const fileLoader = {
        test: /\.(png|jpe?g|gif)$/i,
        type: 'asset/resource',
    };

    const svgLoader = {
        test: /\.svg$/,
        use: [
            {
                loader: '@svgr/webpack',
                options: {
                    icon: true,
                },
            },
        ],
    };
    const cssLoaders = {
        test: /\.s[ac]ss$/i,
        use: [
            // Creates `style` nodes from JS strings
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            // "style-loader",
            // MiniCssExtractPlugin.loader,
            // Translates CSS into CommonJS
            // "css-loader",
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName: isDev ? '[path][name]__[local]' : '[hash:base64:8]',
                        // чтобы не автогенерировать хэш для не module.scss файлов
                        auto: (resPath: string) => Boolean(resPath.includes('.module.')),
                    },
                },
            },
            // Compiles Sass to CSS
            'sass-loader',
        ],
    };
    // порядок ВАЖЕН!
    // порядок с конца
    return [
        cssLoaders,
        fileLoader,
        svgLoader,
        typeScriptLoader,
    ];
}
