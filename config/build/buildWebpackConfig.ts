import { type Configuration } from 'webpack';
import { type BuildOptions } from './types/types';
import { buildLoaders } from './buildLoaders';
import { buildResolvers } from './buildResolvers';
import { buildPlugins } from './buildPlugins';
import { buildDevServer } from './buildDevServer';

export function buildWebpackConfig(options: BuildOptions): Configuration {
    const { mode, paths } = options;

    const isDev = mode === 'development';
    // const isProd = mode === 'production';

    return {
        mode: mode ?? 'development',
        // entry: './src/index.tsx',
        entry: paths.entry,
        output: {
            filename: '[name].[contenthash].js',
            // path: path.resolve(__dirname, 'build'),
            path: paths.output,
            clean: true,
        },
        module: {
            rules: buildLoaders(options),
        },
        devtool: isDev ? 'inline-source-map' : undefined,
        resolve: buildResolvers(options),
        plugins: buildPlugins(options),
        devServer: isDev ? buildDevServer(options) : undefined,
    };
}
