import { type ResolveOptions } from 'webpack';
import { type BuildOptions } from './types/types';

export function buildResolvers(options: BuildOptions): ResolveOptions {
    return {
        extensions: ['.tsx', '.ts', '.js'],
        alias: {
            '@': options.paths.src,
        },
    };
}
