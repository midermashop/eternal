import { lazy } from 'react';

export const LazyAbout = lazy(async () => await import('./About').then((module) => ({
    default: module.About,
})));
