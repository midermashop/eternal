import { lazy } from 'react';

export const LazyAdmin = lazy(async () => await import('./Admin').then((module) => ({
    default: module.Admin,
})));
