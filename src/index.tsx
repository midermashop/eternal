import { createRoot } from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { Suspense } from 'react';
import { App } from './components/App';
import { LazyAbout } from './pages/about/About.lazy';
import { LazyAdmin } from './pages/admin/Admin.lazy';

const root = document.getElementById('root');

const container = createRoot(root);

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        children: [
            {
                path: '/about',
                element: <Suspense><LazyAbout /></Suspense>,
            },
            {
                path: '/admin',
                element: <Suspense><LazyAdmin /></Suspense>,
            },
        ],
    },
]);
container.render(
    <RouterProvider router={router} />,
);
