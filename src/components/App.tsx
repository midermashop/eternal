import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import { Counter } from './Counter';
import avatarPng from '@/assets/avatar.png';
import avatarJpg from '@/assets/avatar.jpg';
import AvatarSvg from '@/assets/avatar.svg';

import '../index.scss';

export function App() {
    return (
        <div className="app">
            <Link to="/about">About</Link>
            <Link to="/admin">Admin</Link>
            TEXTsdsaf

            <div>
                <img height={20} width={20} src={avatarPng} alt="kek" />
                <img height={20} width={20} src={avatarJpg} alt="kek1" />
                <AvatarSvg height={40} width={40} fill="red" />
            </div>
            <Counter />
            <Outlet />
        </div>
    );
}
