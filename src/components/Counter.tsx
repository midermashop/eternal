import React from 'react';
import styles from './Counter.module.scss';

export function Counter() {
    const [count, setCount] = React.useState(0);

    return (
        <div>
            {count}
            <button type="button" className={styles.btn} onClick={() => { setCount((prev) => prev + 1); }}>Clik</button>
        </div>
    );
}
