import path from 'path';
import type webpack from 'webpack';
import { buildWebpackConfig } from './config/build/buildWebpackConfig';
import { type BuildPaths, type EnvVariables } from './config/build/types/types';
// import { BuildPaths } from "config/build/types/types";
// import { buildWebpackConfig } from "config/build/buildWebpackConfig";
/**/

const paths: BuildPaths = {
    output: path.resolve(__dirname, 'build'),
    entry: path.resolve(__dirname, 'src', 'index.tsx'),
    html: path.resolve(__dirname, 'public', 'index.html'),
    // public: path.resolve(__dirname, 'public'),
    src: path.resolve(__dirname, 'src'),
};

export default (env: EnvVariables) => {
    const config: webpack.Configuration = buildWebpackConfig({
        port: env.port ?? 3000,
        mode: env.mode ?? 'development',
        paths,
    // analyzer: env.analyzer,
    // platform: env.platform ?? 'desktop'
    });
    return config;
};
