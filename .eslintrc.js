module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "plugin:react/recommended",
        'airbnb'
    ],
    parser: "@typescript-eslint/parser",
    "parserOptions": {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 'latest',
        sourceType: "module"
    },
    "plugins": [
        "react",
        '@typescript-eslint'
    ],
    "rules": {
        "react/jsx-props-no-spreading": "warn",
        "import/prefer-default-export": 'off',
        "plugin:react/jsx-runtime": 'off',
        "no-return-await": 'off',
        "import/extensions": 'off',
        "import/no-unresolved": 'off',
        "react/jsx-filename-extension": [2, { "extensions": [".js", ".jsx", ".tsx"] }],
        "react/jsx-indent": [2,4],
        "indent": [2,4],
        "react/react-in-jsx-scope": "off",
        "import/no-extraneous-dependencies": "off",
        "max-len": [
            "error",
            {
                // "ignorePattern": "^import |^export {(.*?)}|\\s\\* |(export)? class [a-zA-Z]+(<.*>)? implements |\\/\\/|(export )?function [a-zA-Z]+<([a-zA-z]+ extends .*)*,?>",
                "ignorePattern": "\\/\\/",
                "code": 120
            }
        ],
    }
}
